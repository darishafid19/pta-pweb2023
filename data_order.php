<!DOCTYPE html>
<html>
<head>
  <title>Data Order</title>
  <link rel="stylesheet" href="style_php.css" />
</head>
<body>
    <div class="wraper"><div class="brand">
        <div class="firstname">data</div>
        <div class="lastname">order</div>
      </div>
    <a href="index.html" style="margin-right:5%"> <button type="button" style="width:100px; height: 50px;; border-radius: 20px" >Kembali</button></a></div>
  <div class="container">    
    <table class="table" width="100%" >
      <thead>
        <tr>
          <th>Nama peminjam</th>
          <th>No WA</th>
          <th>Jaminan</th>
          <th>Motor</th>
          <th>Lama peminjaman</th>
        </tr>
      </thead>
      <tbody>
        <?php
        $file = "file.txt";
        if (file_exists($file)) {
          $transaksi = file($file);
          foreach ($transaksi as $data) {
            $row = explode("|", $data);
            $nama = trim($row[0]);
            $no_hp = trim($row[1]);
            $jaminan = trim($row[2]);
             $motor = trim($row[3]);
            $lama = trim($row[4]);
            echo "<tr>";
            echo "<td>$nama</td>";
            echo "<td>$no_hp</td>";
            echo "<td>$jaminan</td>";
            echo "<td>$motor</td>";
            echo "<td>$lama</td>";
            echo "</tr>";
          }
        } else {
          echo "<tr><td colspan='2'>Tidak ada data transaksi.</td></tr>";
        }
        ?>
      </tbody>
    </table>
  </div>
</body>
</html>
