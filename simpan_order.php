<?php
if(isset($_POST['nama']) && isset($_POST['no_hp']) && isset($_POST['jaminan']) && isset($_POST['motor']) && isset($_POST['lama'])){
    $nama = $_POST['nama'];
    $no_hp = $_POST['no_hp'];
    $jaminan = $_POST['jaminan'];
    $motor = $_POST['motor'];
    $lama = $_POST['lama'];

    $file = fopen('file.txt', 'a+');
    fwrite($file, "$nama | $no_hp | $jaminan | $motor | $lama\n");
    fclose($file);

    $pesan = "Data berhasil disimpan!";
} else {
    $pesan = "Terjadi kesalahan dalam menyimpan Data.";
}
?>
<!DOCTYPE html>
<html>
<head>
    <title>Simpan Data Order</title>
    <link rel="stylesheet" href="style.css" />
</head>
<body>
    <nav class="wraper">
      <div class="brand">
        <div class="firstname">sewa</div>
        <div class="lastname">motor</div>
      </div>
      <ul class="navigasi">
        <li><a href="data_order.php">Data Order</a></li>
      </ul>
    </nav>
    <div class="container_php">
    <div class="content">
        <form method="POST" action="proses.php">
            <table width="100%" align="center">
                <tr>
                    <td colspan="2">
                        <h2><?php echo $pesan; ?></h2>
                    </td>
                </tr>
                <tr rowspan="2">
                    <td colspan="2">
                        <a href="index.html" style="text-decoration: none; font-weight: bolder;">
                            <button type="button" >Kembali</button>
                        </a>
                    </td>
                </tr>
            </table>
        </form>
    </div>
    </div>

    <script src="js/script.js"></script>
</body>
</html>
