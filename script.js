// Menyimpan keadaan tampilan awal card saat halaman dimuat
var originalDisplayStates = [];

// Mendapatkan semua elemen card
var cards = document.querySelectorAll(".card");

// Menyimpan keadaan tampilan awal card
cards.forEach(function (card) {
  originalDisplayStates.push(card.style.display);
});

function search() {
  // Mendapatkan nilai input pencarian
  var searchInput = document.querySelector(".input").value.toLowerCase();

  // Melakukan iterasi untuk setiap elemen card
  cards.forEach(function (card, index) {
    var title = card.querySelector(".motor").textContent.toLowerCase();

    // Memeriksa apakah judul card mengandung kata kunci pencarian
    if (title.includes(searchInput)) {
      card.style.display = originalDisplayStates[index]; // Menampilkan card jika cocok
    } else {
      card.style.display = "none"; // Menyembunyikan card jika tidak cocok
    }
  });
}

// Menambahkan event listener pada input pencarian
var searchInput = document.querySelector(".input");
searchInput.addEventListener("keyup", search);

function validateForm() {
  // Memeriksa apakah nama telah diisi
  var nama = document.getElementById("nama").value;
  if (nama == "") {
    alert("Nama harus diisi");
    return false;
  }

  // Memeriksa apakah nomor HP telah diisi
  var no_hp = document.getElementById("no_hp").value;
  if (no_hp == "") {
    alert("Nomor HP harus diisi");
    return false;
  }

  // Memeriksa apakah jaminan telah dipilih
  var jaminan = document.getElementById("jaminan").value;
  if (jaminan == "") {
    alert("Jaminan harus dipilih");
    return false;
  }

  // Memeriksa apakah motor telah dipilih
  var motor = document.getElementById("motor").value;
  if (motor == "") {
    alert("Motor harus dipilih");
    return false;
  }

  // Memeriksa apakah lama pinjam telah diisi
  var lama = document.getElementById("lama").value;
  if (lama == "") {
    alert("Lama pinjam harus diisi");
    return false;
  }

  return true; // Mengizinkan pengiriman formulir jika semua validasi terlewati
}
